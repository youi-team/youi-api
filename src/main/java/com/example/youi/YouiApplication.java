package com.example.youi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class YouiApplication {

    public static void main(String[] args) {
        SpringApplication.run(YouiApplication.class, args);
    }

}
